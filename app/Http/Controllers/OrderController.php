<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderDetails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = $this->getAuthenticatedUser();
        $orders =DB::table('orders as or')
            ->join('customers as c','c.id','=','or.customer_id')
            ->select('or.id','c.name as customer','or.created_at as date')
            ->orderBy('customer','desc')
            ->where('or.user_id','=',$user->id)
            ->get();
        return response()->json($orders,200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validate = Validator::make($request->all(),[
            'customer_id' => 'required'
        ]);

        if($validate->fails()){
            return response()->json(
                $validate->errors(),400);
        }else{
            $user = $this->getAuthenticatedUser();
            $order = new Order([
                'customer_id' => $request->input('customer_id'),
                'user_id' => $user->id
            ]);
            $order->save();
            return response()->json([
                'message'=>'Created order!'],201);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $orderDetails =DB::table('order_details as or')
            ->join('products as p','p.id','=','or.product_id')
            ->select('or.id','p.name as product','or.quantity')
            ->orderBy('product','desc')
            ->where('or.order_id','=',$id)
            ->get();
        return response()->json($orderDetails,200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validate = Validator::make($request->all(),[
            'customer_id' => 'required'
        ]);

        if($validate->fails()){
            return response()->json(
                $validate->errors(),400);
        }else{
            Order::findOrFail($id)->update([
                'customer_id' => $request->input('customer_id')
            ]);
            return response()->json([
                'message'=>'Update order!'],200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Order::findOrFail($id)->delete();
        return response()->json([
            'message'=>'Delete order!'],200);
    }

    public function getAuthenticatedUser()
    {
        try {

            if (! $user = JWTAuth::parseToken()->authenticate()) {
                return response()->json(['user_not_found'], 404);
            }

        } catch (JWTException $e) {

            return response()->json(['token_error'], $e->getStatusCode());

        // the token is valid and we have found the user via the sub claim
        }
        return $user;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function storeDetails(Request $request,$id)
    {
        $validate = Validator::make($request->all(),[
            'product_id' => 'required',
            'quantity' => 'required'
        ]);

        if($validate->fails()){
            return response()->json(
                $validate->errors(),400);
        }else{
            $orderDetail = new OrderDetails([
                'order_id' => $id,
                'product_id' => $request->input('product_id'),
                'quantity' => $request->input('quantity')
            ]);
            $orderDetail->save();
            return response()->json([
                'message'=>'Created orderDetails!'],201);
        }
    }
    public function destroyDetails($id)
    {
        OrderDetails::findOrFail($id)->delete();
        return response()->json([
            'message'=>'Delete orderDetail!'],200);
    }
}
