<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Facades\JWTAuth;

class AuthController extends Controller
{
    public function signup(Request $request){

       $validate = Validator::make($request->all(),[
           'name' => 'required',
           'email' => 'required|email|unique:users',
           'password' => 'required'
       ]);

       if($validate->fails()){
           return response()->json(
               $validate->errors(),400);
       }else{
           $user = new User([
               'name' => $request->input('name'),
               'email' => $request->input('email'),
               'password' =>bcrypt($request->input('password'))
           ]);
           $user->save();
           return response()->json([
               'message'=>'Created user'],201);
       }
    }// end signup
    public function signin(Request $request){
        $validate = Validator::make($request->all(),[
            'email' => 'required|email',
            'password' => 'required'
        ]);

        if($validate->fails()){
            return response()->json(
                $validate->errors(),400);
        }else{
            $credentials = $request->only('email','password');
            try{
                if(!$token = JWTAuth::attempt($credentials)){
                    return response()->json([
                        'error'=>'Invalid Credentials!'],401);
                }
            }catch (JWTException $e){
                return response()->json([
                    'error'=>'Could not create token!'],500);
            }
            $currentUser = Auth::user();
            return response()->json([
                'token'=>$token,'user'=>$currentUser->name],200);
        }
    }
}
