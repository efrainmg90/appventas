<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /**
* The attributes that are mass assignable.
*
* @var array
*/
    protected $table = 'orders';
    protected $fillable = [
        'customer_id','user_id'
    ];
}
