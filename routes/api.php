<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('/signup',['uses'=>'AuthController@signup']);
Route::post('/signin',['uses'=>'AuthController@signin']);

Route::group(['middleware' => 'jwt.auth'],function(){
    Route::resource('orders', 'OrderController');
});
Route::resource('customers', 'CustomerController');

Route::resource('routes', 'RouteController');

Route::resource('products', 'ProductController');

Route::post('/orders/{id}',['uses'=>'OrderController@storeDetails']);
Route::delete('/orders/details/{id}',['uses'=>'OrderController@destroyDetails']);
